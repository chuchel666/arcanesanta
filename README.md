Simple secret Santa bot for Telegram.

Run:

* `pip3 install -r requirements.txt`
* `python3 santa.py create`
* `python3 santa.py`

Optionally you can use `venv` to keep your system clean of dependency packages and `torsocks` to bypass the government censorship.

Usage:

1. Add bot to specific group with all your friends in it.
2. Everyone has to register by `/register` command and mark themselves ready by `/ready`
3. If all users are ready the bot will generate pares and send it to every user. Note that Telegram API is prohibiting sending messages from a bot to user that not started conversation with the bot before
4. Get your target to gift for by `target` command in a private chat with the bot

Licensed under [![WTFPL Logo](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png)](http://www.wtfpl.net/)