import telegram
import sqlite3
import yaml
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
import sys
import logging
import random

class Santa:
    def __init__(self, config):
        self.config = config
        self.updater = Updater(config['token'], use_context=True)
        self.dispatcher = self.updater.dispatcher

        self.dispatcher.add_handler(CommandHandler('start', self.start))
        self.dispatcher.add_handler(CommandHandler('register', self.register))
        self.dispatcher.add_handler(CommandHandler('count', self.count))
        self.dispatcher.add_handler(CommandHandler('ready', self.ready))
        self.dispatcher.add_handler(CommandHandler('target', self.target))

    def start_polling(self):
        self.updater.start_polling()

    def start(self, update, cb):

        try:
            update.message.reply_text("Yo-ho-ho! Usage: /register : use only in chat group; register a new player. /ready : let Santa know you are ready to become Santa. Don't forget /start with me in a private chat to let me chat with you")
        except Exception as e:
            print(e)

    def register(self, update, cb):
        if update.message.chat.type == 'private':
            update.message.reply_text('Please use this command in group chat only')
            return

        user_id = update.message.from_user.id
        group_id = update.message.chat_id

        with sqlite3.connect('santa.db') as conn:
            c = conn.cursor()
            c.execute('SELECT * from players WHERE id=?', (user_id,))
            if c.fetchone() is not None:
                update.message.reply_text('You are already registered')
                return
            c.execute('INSERT INTO players VALUES (?, ?, NULL, 0)', (user_id, group_id))

        update.message.reply_text('Registered!')

    def count(self, update, cb):
        if update.message.chat.type == 'private':
            update.message.reply_text('Please use this command in group chat only')
            return

        group_id = update.message.chat_id

        try:
            with sqlite3.connect('santa.db') as conn:
                c = conn.cursor()

                c.execute('SELECT COUNT(id) FROM players WHERE group_id=?', (group_id,))
                total = c.fetchone()[0]

                c.execute('SELECT count(id) FROM players WHERE ready=1 AND group_id=?', (group_id,))
                ready = c.fetchone()[0]

            update.message.reply_text('Total: {}, Ready: {}'.format(total, ready))
        except Exception as e:
            print(e)

    def ready(self, update, cb):
        if update.message.chat.type == 'private':
            update.message.reply_text('Please use this command in group chat only')
            return

        user_id = update.message.from_user.id
        group_id = update.message.chat_id

        try:
            with sqlite3.connect('santa.db') as conn:
                c = conn.cursor()
                c.execute('SELECT id FROM players WHERE id=?', (user_id,))
                users = c.fetchone()
                if users is None or len(users) == 0:
                    update.message.reply_text('Sorry, but you are not registered yet. Please, run /register command in a chat group')
                    return

                c.execute('UPDATE players SET ready=1 WHERE id=?', (user_id,))
                c.execute('SELECT count(id) FROM players WHERE group_id=?', (group_id,))
                total = c.fetchone()[0]
                c.execute('SELECT id FROM players WHERE ready=1 AND group_id=?', (group_id,))
                ready = c.fetchall()

                if len(ready) == total:
                    print("ready: {}".format(ready))
                    targets = self.roll([x[0] for x in ready])
                    print(targets)
                    for user in targets:
                        try:
                            c.execute('SELECT target FROM players WHERE id=?', (user['id'],))
                            t = c.fetchone()
                            if t is not None and t[0] != None:
                                print("target is set and equals to: {}".format(t))
                                target = t[0]

                            else:
                                target = user["target"]
                                c.execute('UPDATE players SET target=? WHERE id=? AND target IS NULL', (user["target"],user["id"] ))

                            self.send_link(target, "Your target", cb.bot.send_message, chat_id=user["id"])

                        except Exception as ae:
                            print("Error: {}, {}".format(ae, user["id"]))
                    update.message.reply_text("Exellent! Everybody seem to be ready. Sending targets")
                else:
                    update.message.reply_text('Done! You are ready to become Santa now!')

        except Exception as e:
            print(e)

    def target(self, update, cb):
        if update.message.chat.type != 'private':
            update.message.reply_text('Please use this command in private chat only')
            return

        user_id = update.message.from_user.id

        try:
            with sqlite3.connect('santa.db') as conn:
                c = conn.cursor()
                c.execute('SELECT target FROM players WHERE id=?', (user_id,))
                target = c.fetchone()
            if target[0] is None:
                update.message.reply_text("There is no target for you yet, yong Santa!")
            else:
                self.send_link(target[0], "Your target", update.message.reply_text)

        except Exception as e:
            print(e)


    def roll(self, player_ids):
        random.shuffle(player_ids)
        print("player_ids: {}".format(player_ids))

        result = [{"id":p, "target":player_ids[i+1]} for i,p in enumerate(player_ids[:-1])]
        result.append({"id" : player_ids[-1], "target" : player_ids[0]})

        return result


    def send_link(self, target_id, text, callback, chat_id=None):
        link = "[{}](tg://user?id={})".format(text, target_id)

        if chat_id is None:
            callback(text=link, parse_mode=telegram.ParseMode.MARKDOWN)
        else:
            callback(text=link, parse_mode=telegram.ParseMode.MARKDOWN, chat_id=chat_id)

def main():
    log = logging.getLogger('santa')
    logger = logging.StreamHandler(sys.stdout)
    logger.setFormatter(logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s'))
    logger.setLevel(logging.DEBUG)
    log.addHandler(logger)
    log.setLevel(logging.DEBUG)

    with open('config.yaml', 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    if len(sys.argv) > 1:
        if sys.argv[1] == 'create':
            with open('init.sql', 'r') as f:
                conn = sqlite3.connect('santa.db')
                c = conn.cursor()
                lines = f.readlines()
                for line in lines:
                    c.execute(line)
            sys.exit(0)

    santa = Santa(config)
    santa.start_polling()

if __name__ == '__main__':
    main()
